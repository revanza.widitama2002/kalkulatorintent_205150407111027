package com.example.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class halaman2 extends AppCompatActivity implements View.OnClickListener {

//mendefinisikan button dan Textview
    TextView angka;
    TextView hasiloperasi;
    Button btback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//men-set konten view untuk dihubungkan dengan layout
        setContentView(R.layout.halaman2);

////mendeklarasikan variabel untuk dihubungan dengan id pada layout
        angka = findViewById(R.id.hasil);
        hasiloperasi = findViewById(R.id.operasi);
        btback = findViewById(R.id.btback);

//Menghubungkan button dengan OnCLickListener
        btback.setOnClickListener(this);

//Mengambil intent, string "Hasil" dan "Operasi" untuk mengambil data pada Halaman pertama / activity sebelumnya
        Intent intent = getIntent();
        String hasil = intent.getStringExtra("Hasil");
        String operasi = intent.getStringExtra("Operasi");

//Menampilkan operasi dan hasil operasi di layout, pada textview 'hasil', dan 'operasi'
        angka.setText(hasil);
        hasiloperasi.setText(operasi);
    }

    @Override
//Men-setting button untuk menampilkan halaman sebelumnya
    public void onClick(View view) {
        if (view.getId() == R.id.btback) {
            finish();
        }
    }
}